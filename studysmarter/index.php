<?php
	require_once('../../config.php');
	defined('MOODLE_INTERNAL') || die();	
	global $DB ,$USER,$CFG,$PAGE;
	$type = optional_param('ttype',0,PARAM_RAW);
	$password = optional_param('password',0,PARAM_RAW);
	$email = optional_param('email',0,PARAM_RAW);
	$courseid = optional_param('courseid',0,PARAM_RAW);
	$fileid = optional_param('file',0,PARAM_RAW);
	$subject = optional_param('subject',0,PARAM_RAW);
	$urltogo= new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$courseid));
	if($type == "login"){
		$ch = curl_init();
		$base_url=$CFG->local_studysmarter_url;
		curl_setopt($ch, CURLOPT_URL,$base_url."api-token-auth/");
		curl_setopt($ch, CURLOPT_POST, 1);
		$request = "username=".$email."&password=".$password;
		curl_setopt($ch, CURLOPT_POSTFIELDS,$request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$ss_responce = curl_exec($ch);
		curl_close ($ch);
		$token=json_decode($ss_responce);
		if(isset($token->token)){
			$records = new stdClass();
			$records->userid= $USER->id;
			$records->ss_token = $token->token;
			$records->ss_userid = $token->id;
			$DB->insert_record('studysmarter', $records);
			$msg =get_string("loginsuccessful", 'local_studysmarter');
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_SUCCESS);
			}else{
			$msg ="wrong login info";
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_ERROR);
		}
	}
	if($type == "upload"){
		$ss_user=$DB->get_record('studysmarter', array ('userid'=>$USER->id), $fields='*', $strictness=IGNORE_MISSING);
		$filecontextid=$DB->get_record('context', array ('contextlevel'=>70 , 'instanceid'=>$fileid), $fields='*', $strictness=IGNORE_MISSING);
		$select = "contextid = ".$filecontextid->id." AND filesize > 0";
		$filelocation=$DB->get_record_select('files', $select);
		$fcontent = "";
		$fs = get_file_storage();
		$file = $fs->get_file($filelocation->contextid,
		$filelocation->component,
		$filelocation->filearea,
		$filelocation->itemid,
		$filelocation->filepath,
		$filelocation->filename);
		if ($file) $fcontent = $file->get_content();
		$fileext = "." . pathinfo($filelocation->filename, PATHINFO_EXTENSION);
		$filedata = array("TestFile", basename($filelocation->filename), $fcontent);
		$dataFile = $CFG->dataroot.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.'filestorage'.DIRECTORY_SEPARATOR.$filelocation->filename;
		file_put_contents($dataFile, $filedata[2]);
		$fileurl=$CFG->wwwroot."/local/studysmarter/temp_upload/".$filelocation->filename."&name=".$filelocation->filename;
		$target_url=$CFG->local_studysmarter_url."/users/".$ss_user->ss_userid."/subjects/".$subject."/slidesets/";	
		$token=$ss_user->ss_token;
		if (function_exists('curl_file_create')) { 
			$cFile = curl_file_create($dataFile);
			} else { // 
			$cFile = '@' . realpath($dataFile);
		}
		$post = array('subject'=>$subject,'name' => $filelocation->filename,'file_url'=> $cFile);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Token '.$token));
		curl_setopt($ch, CURLOPT_URL,$target_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result=curl_exec ($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		if ($httpcode == '201'){
			unlink($dataFile);
			$msg =get_string("fileuploaded", 'local_studysmarter');
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_SUCCESS);
			}else{
			unlink($dataFile);
			$msg =get_string("Filewasnotuploded", 'local_studysmarter');
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_ERROR);
		}
	}
	if($type == "upload-all"){
		$rand=substr(md5(rand(0,32000)),0,10);
		$ss_user=$DB->get_record('studysmarter', array ('userid'=>$USER->id), $fields='*', $strictness=IGNORE_MISSING);
		$sorted = array();
		$cms = array();
		$modinfo = get_fast_modinfo($courseid);
		foreach ($modinfo->cms as $cm) {
            if (!$cm->uservisible) {
                continue;
			}
            if (!$cm->has_view() && $cm->modname != 'folder') {
                continue;
			}
            $cms[$cm->id] = $cm;
            $resources[$cm->modname][] = $cm->instance;
		}
		$zip = new ZipArchive();
		$zipFile = $CFG->dataroot.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.'filestorage'.DIRECTORY_SEPARATOR.$rand."_".$USER->id.'.zip';
        foreach ($cms as $cm) {
			$inrand=mt_rand(10,100);
            if($cm->modname =="resource"){
				$res = new stdClass;
				$filecontextid=$DB->get_record('context', array ('contextlevel'=>70 , 'instanceid'=>$cm->id), $fields='*', $strictness=IGNORE_MISSING);
				$select = "contextid = ".$filecontextid->id." AND filesize > 0";
				$filelocation=$DB->get_record_select('files', $select);
				if($filelocation->mimetype =="application/pdf"){
					$fcontent = "";
					$fs = get_file_storage();
					$file = $fs->get_file($filelocation->contextid,
					$filelocation->component,
					$filelocation->filearea,
					$filelocation->itemid,
					$filelocation->filepath,
					$filelocation->filename);
					if ($file) $fcontent = $file->get_content();
					$fileext = "." . pathinfo($filelocation->filename, PATHINFO_EXTENSION);
					$filedata = array("TestFile", basename($filelocation->filename), $fcontent);
					$dataFile = $CFG->dataroot.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.'filestorage'.DIRECTORY_SEPARATOR.$inrand."_".$filelocation->filename;
					file_put_contents($dataFile, $filedata[2]);
					if ($zip->open($zipFile, ZipArchive::CREATE)) {
						$zip->addFile($dataFile,$inrand."_".$filelocation->filename);
					}
					$zip->close();
					unlink($dataFile);
				}
			}
		}
		$target_url=$CFG->local_studysmarter_url."/users/".$ss_user->ss_userid."/subjects/".$subject."/tempfiles/";	
		$token=$ss_user->ss_token;
		if (function_exists('curl_file_create')) { 
			$cFile = curl_file_create($zipFile);
			} else { // 
			$cFile = '@' . realpath($zipFile);
		}
		$post = array('subject'=>$subject,'file_url'=> $cFile);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Token '.$token));
		curl_setopt($ch, CURLOPT_URL,$target_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result=curl_exec ($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		print_object($result);
		curl_close ($ch);
		if ($httpcode == '201'){
			unlink($zipFile);
			$msg =get_string("fileuploaded", 'local_studysmarter');
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_SUCCESS);
			}else{
			unlink($zipFile);
			$msg =get_string("Filewasnotuploded", 'local_studysmarter');
			redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_ERROR);
		}
	}
	if($type == "logout"){
		$DB->delete_records('studysmarter', array('userid'=> $USER->id));
		$msg =get_string("Loggedout", 'local_studysmarter');
		redirect($urltogo,$msg,null, \core\output\notification::NOTIFY_SUCCESS);
	}
	
	if($type == "enable"){
		$DB->delete_records('studysmarter_status', array('courseid'=>$courseid));
		//$msg =get_string("Loggedout", 'local_studysmarter');
		redirect($urltogo);
	}
	
	if($type == "disable"){
		//$DB->delete_records('studysmarter_status', array('$courseid'=> $USER->id));
		$records = new stdClass();
		$records->courseid= $courseid;
		$records->enable= 0;
		$DB->insert_record('studysmarter_status', $records);
		//$msg =get_string("Loggedout", 'local_studysmarter');
		redirect($urltogo);
	}
?>