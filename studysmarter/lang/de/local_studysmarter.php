<?php
	$string['studysmarterurlhelp']='StudySmarter API url';
	$string['studysmarterurllabel']='StudySmarter API url';
	$string['settingstitle']='StudySmarter-Einstellungen';
	$string['fileuploaded'] ='Dein Dokument wurde hochgeladen!';
	$string['Filewasnotuploded']='Dein Dokument konnte nicht hochgeladen werden.';
	$string['Loggedout']='Ausgelogged';
	$string['Logintext']='Login bei StudySmarter';
	$string['loginsuccessful']='Erfolgreich angemeldet';
	$string['Buttontext']='Zu StudySmarter hochladen';
	$string['Uploadalltxt']='Alle Dokumente hochladen';
	$string['Logouttxt']='Logout von StudySmarter';
	$string['Youremail']='Deine Email';
	$string['Signin']='Login';
	$string['Signup']='Registrieren';
	$string['PleaseSelect']='Wähle ein Fach aus';
	$string['Yourpassword']='Dein Passwort';	
	$string['selectsubject']='Fach auswählen';	
	$string['studysmarterenablelabel']='Studysmarter aktivieren';	
	$string['studysmarterenablehelp']='Studysmarter an / aus';	
	$string['loginfirst']='Bitte melde dich zuerst via StudySmarter an.';	
	$string['loginhelp']='Logge dich hier mit deinem StudySmarter-Account ein oder registriere dich, um Dokumente direkt in die Smart-Learning-Plattform zu importieren';	
	$string['logindiscription']='Logge dich hier mit deinem StudySmarter-Account ein oder registriere dich';	//can have html format
	$string['disable']='StudySmarter deaktivieren';
	$string['enable']='StudySmarter aktivieren';
	$string['studysmarter:manage']='Manage StudySmarter';
	$string['pluginname'] = 'StudySmarter';
?>