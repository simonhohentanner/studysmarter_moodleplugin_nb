<?php
	$string['studysmarterurlhelp']='StudySmarter API url';
	$string['studysmarterurllabel']='StudySmarter API url';
	$string['settingstitle']='StudySmarter Settings';
	$string['fileuploaded'] ='Your file was successfully uploaded to StudySmarter.';
	$string['Filewasnotuploded']='Your file could not be uploaded';
	$string['Loggedout']='Logged out';
	$string['Logintext']='Login to StudySmarter';
	$string['loginsuccessful']='Login successful';
	$string['Buttontext']='Upload to StudySmarter';
	$string['Uploadalltxt']='Upload all files';
	$string['Logouttxt']='Logout from StudySmarter';
	$string['Youremail']='Your email';
	$string['Signin']='Sign in';
	$string['Signup']='Sign up';
	$string['PleaseSelect']='Select a subject';
	$string['Yourpassword']='Your password';	
	$string['selectsubject']='Select Subject';	
	$string['studysmarterenablelabel']='Enable Studysmarter';	
	$string['studysmarterenablehelp']='Enable/Disable Studysmarter';	
	$string['loginfirst']='Please login to StudySmarter first.';	
	$string['loginhelp']='Login with your StudySmarter account or create a new one.';	
	$string['logindiscription']='Login with your StudySmarter account or create a new one in order to import files directly to the smart studying platform.';	//can have html format
	$string['enable']='Enable StudySmarter';	
	$string['disable']='Disable StudySmarter';	
	$string['studysmarter:manage']='Manage StudySmarter';
	$string['pluginname'] = 'StudySmarter';
?>