function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}

function endload(lang,subjectlist,logged,moodlebase,courseid){
	
	var logintext =lang['Logintext'];
	var buttontext = lang['Buttontext'];
	var uploadalltxt = lang['Uploadalltxt'];
	var logouttxt = lang['Logouttxt'];
	var Youremail = lang['Youremail'];
	var Signin = lang['Signin'];
	var Signup = lang['Signup'];
	var PleaseSelect = lang['PleaseSelect'];
	var Yourpassword = lang['Yourpassword'];
	var selectsubject = lang['selectsubject'];
	var loginfirst = lang['loginfirst'];
	var loginhelp = lang['loginhelp'];
	var logindiscription = lang['logindiscription'];
	
	
	$("img[src$='pdf-24']").each(function () {
		
		var linkname= $(this).parent();
		var url = (linkname.attr('href'));
		var id = getURLParameter(url, 'id');
		$( '<span class="studysmarterupload">	<a class="btn btn-primary btn-sm ssupload" data-id="'+id+'" href="#" role="button">'+buttontext+'</a>').insertAfter( linkname );
	});
	var navparent= $("#page-navbar").parent();
	if (logged == true ){
		var selectlist;
		$.each(subjectlist, function(i, item) {
			selectlist =  selectlist +'<option value="'+i+'">'+item+'</option>';
		});
		
		$( '<span class="subjectselectspan"><select name="subject" id="subject"><option value="0">'+PleaseSelect+'</option>'+selectlist+'</select></span>' ).insertBefore( navparent );
		
		$( '<span class="uploadallspan"><button type="button" class="btn btn-primary btn-sm uploadall">'+uploadalltxt+'</button></span>' ).insertBefore( navparent );
		
		$( '<span class="logoutspan"><a class="btn btn-primary btn-sm logoutbtn"  href="'+moodlebase+'/local/studysmarter/index.php?ttype=logout&courseid='+courseid+'" role="button">'+logouttxt+'</a></span>' ).insertBefore( navparent );
		
		
		}else{
		var selectsubject = loginfirst;
		$( '<span class="subjectselectspan" style="display:none"><select name="subject" id="subject"><option value="0">'+PleaseSelect+'</option></select></span>' ).insertBefore( navparent );
		$( '<span class="loginbtnspan"><button type="button" class="btn btn-primary btn-sm ssloginbtn" >'+logintext+'</button><a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="'+logindiscription+'" data-html="true" tabindex="0" data-trigger="focus">  <i class="icon fa fa-question-circle text-info fa-fw " aria-hidden="true" title="'+logintext+'" aria-label="'+logintext+'"></i></a></span>' ).insertBefore( navparent );
		var loginform ='<div id="sslogincard" class="card" style="display:none; max-width:50%">'
		+'<article class="card-body">'
		+'<a href="https://app.studysmarter.de/register?source=moodle" class="float-right btn btn-outline-primary" target="_blank">'+Signup+'</a>'
		+'<h4 class="card-title mb-4 mt-1">'+Signin+'</h4>'
		+'	 <form id="LoginForm" action= "'+moodlebase+'/local/studysmarter/index.php" method="post">'
		+'    <div class="form-group">'
		+'    	<label>'+Youremail+'</label>'
		+'        <input name="email" id="email" class="form-control required" placeholder="Email" type="email">'
		+'    </div> <!-- form-group// -->'
		+'    <div class="form-group">'
		+'    	<label>'+Yourpassword+'</label>'
		+'        <input class="form-control required" placeholder="******" type="password" name="password" id="password">'
		+'        <input type="hidden" name="ttype" value="login">'
		+'        <input type="hidden" name="courseid" value="'+courseid+'">'
		
		+'    </div> <!-- form-group// --> '
		+'    <div class="form-group"> '
		+'    </div> <!-- form-group// -->  '
		+'    <div class="form-group">'
		+'        <button type="submit" class="btn btn-primary btn-block sslogin">'+logintext+'</button>'
		+'    </div> <!-- form-group// -->                                                           '
		+'</form>'
		+'</article>'
		+'</div>';
		$( '<span class="loginform">'+loginform+'</span>' ).insertBefore( navparent );
	}
	
	$('.ssupload').on('click', function(event) {
		event.preventDefault();
		var subject = $('#subject').find(":selected").val();
		var did = ($(this).attr('data-id'));
		var gotourl = moodlebase+'/local/studysmarter/index.php?ttype=upload&file='+did+'&courseid='+courseid+'&subject='+subject;
		if(subject == 0){
			alert(selectsubject);	
			if(selectsubject == loginfirst){
			$( "#sslogincard" ).toggle();
			}
			}else{
			location.href = gotourl;
		}
	});
	
	$('.uploadall').on('click', function(event) {
		event.preventDefault();
		var subject = $('#subject').find(":selected").val();
		var gotourl = moodlebase+'/local/studysmarter/index.php?ttype=upload-all&courseid='+courseid+'&subject='+subject;
		if(subject == 0 ){
			alert(selectsubject);	
			}else{
			location.href = gotourl;
		}
	});
	
	$('.ssloginbtn').on('click', function(event) {
		event.preventDefault();
		$( "#sslogincard" ).toggle();
	});
	
$('[data-toggle="studysmarters"]').on('click', function(event) {
		event.preventDefault();
		alert('');
});
	
$(function () {
  $('[data-toggle="studysmarter"]').popover(); 
})
	
	
	
	
	
};