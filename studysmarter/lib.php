<?php
	defined('MOODLE_INTERNAL') || die();
	

function local_studysmarter_extend_settings_navigation(settings_navigation $settingsnav, context $context) {
$usercan= has_capability('local/studysmarter:manage', $context);

	global $PAGE,$DB;
		$ss_status=$DB->get_record('studysmarter_status', array ('courseid'=>$PAGE->course->id), $fields='*', $strictness=IGNORE_MISSING);
		if($ss_status){
		if ($ss_status->enable == 1){
		$case='disable';
		$icon ='local_studysmarter_off';
		}else{
		$case='enable';
		$icon ='local_studysmarter_on';
		}
		}else{
		$case='disable';
		$icon ='local_studysmarter_off';
		}
$url = new moodle_url('/local/studysmarter/index.php', array('courseid' => $PAGE->course->id,'ttype' => $case));
        $title = get_string($case, 'local_studysmarter');		
$coursenode = $settingsnav->get('courseadmin');
$pix = new pix_icon('icon', $title, $icon);
if ($coursenode && $usercan) {
$coursenode->add($title, $url, '',null, '',$pix);
}

}
	
function local_studysmarter_extend_navigation(global_navigation $nav) {
		global $PAGE,$DB;
		
		if ($PAGE->pagetype == 'course-view-topics') {
			$rand=substr(md5(rand(0,32000)),0,10);
			//$PAGE->requires->jquery_plugin('studysmarter-jquerymodule', 'local_studysmarter');
			$PAGE->requires->jquery();
			$PAGE->requires->js('/local/studysmarter/jquery/jquerymodule/studysmarter.js?'.$rand);
		}
		
	
	}	
	
	function local_studysmarter_before_footer() {
		global $PAGE , $DB ,$USER,$CFG, $COURSE;
		$ss_status=$DB->get_record('studysmarter_status', array ('courseid'=>$PAGE->course->id), $fields='*', $strictness=IGNORE_MISSING);
		if($ss_status){
		if ($ss_status->enable == 1){
		$case='disable';
		}else{
		$case='enable';
		}
		}else{
		$case='disable';
		}
		if ($PAGE->pagetype == 'course-view-topics' && $CFG->local_studysmarter_enable ==1 && $case=='disable' ) {
			$subjectlis=array();
			$lang=array();
			$lang['Logintext']=get_string('Logintext', 'local_studysmarter');
			$lang['Buttontext']=get_string('Buttontext', 'local_studysmarter');
			$lang['Uploadalltxt']=get_string('Uploadalltxt', 'local_studysmarter');
			$lang['Logouttxt']=get_string('Logouttxt', 'local_studysmarter');
			$lang['Youremail']=get_string('Youremail', 'local_studysmarter');
			$lang['Signin']=get_string('Signin', 'local_studysmarter');
			$lang['Signup']=get_string('Signup', 'local_studysmarter');
			$lang['PleaseSelect']=get_string('PleaseSelect', 'local_studysmarter');
			$lang['Yourpassword']=get_string('Yourpassword', 'local_studysmarter');
			$lang['selectsubject']=get_string('selectsubject', 'local_studysmarter');
			$lang['loginhelp']=get_string('loginhelp', 'local_studysmarter');
			$lang['loginfirst']=get_string('loginfirst', 'local_studysmarter');
			$lang['logindiscription']=get_string('logindiscription', 'local_studysmarter');
			$langobject=json_encode($lang);
			$ss_user=$DB->get_record('studysmarter', array ('userid'=>$USER->id), $fields='*', $strictness=IGNORE_MISSING);
			if(isset($ss_user->ss_token)){
				$subjects_call = ss_api_call('users/'.$ss_user->ss_userid.'/subjects/',null,'subjects',$ss_user->ss_token);
				$subjects=json_decode($subjects_call);
				foreach($subjects->results as $subject){
					$subjectlis[$subject->id]=$subject->name;
				}
				$PAGE->requires->js_init_code("endload(".$langobject.",".json_encode($subjectlis).",true,'".$CFG->wwwroot."','".$COURSE->id."');");
				}else{
				//
				$PAGE->requires->js_init_code("endload(".$langobject.",".json_encode($subjectlis).",false,'".$CFG->wwwroot."','".$COURSE->id."');");
			}
		}
	}
	
		function ss_api_call($url,$postfields,$type,$token){
		global $CFG;
		$ch = curl_init();
		$base_url=$CFG->local_studysmarter_url;
		curl_setopt($ch, CURLOPT_URL,$base_url.$url);
		if($type="subjects"){
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token '.$token,
			));
			}else{
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($postfields));
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$ss_responce = curl_exec($ch);
		curl_close ($ch);
		return $ss_responce;
	}
	
	function local_studysmarter_get_fontawesome_icon_map() {
    return [
        'local_studysmarter_on:icon' => 'fa-check-circle',
        'local_studysmarter_off:icon' => 'fa-times-circle',
    ];
}
	
?>